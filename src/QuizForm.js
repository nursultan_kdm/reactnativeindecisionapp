
let visible = false;
let visibleState;
changeVisibility = (visible) => {
    if(visible) {
        visibleState = 'display: block';
    } else {
        visibleState = 'display: none';
    }
};

const template = (
    <div>
        <h1>Visibility Toggle</h1>
        <button onClick={changeVisibility}>{visible ? 'Hide' : 'Show'}</button>
        <p style={visibleState}>Hey a im a text</p>
    </div>
);
