
const getFirstName = (x) => {
    return x.split(' ')[0];
};

const getFirstNameArrow = (x) => x.split(' ')[0];

const multiplier = {
    numbers: [5, 10, 15],
    multiplyBy: 2,
    multiply() {
        return this.number.map((number) => number * this.multiplyBy);
    }
};