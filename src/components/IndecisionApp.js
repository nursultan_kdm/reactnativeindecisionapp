import React from 'react';
import AddOption from './AddOption';
import Action from './Action';
import Header from './Header';
import Options from './Options';
import OptionModal from './OptionModal';

export default class IndecisionApp extends React.Component {
    state = {
        options: [],
        selectedOption: undefined
    };
    // constructor(props) {
    //     super(props);
    //     this.handleDeleteOptions = this.handleDeleteOptions.bind(this);
    //     this.handlePick = this.handlePick.bind(this);
    //     this.handleAddOption = this.handleAddOption.bind(this);
    //     this.handleDeleteOption = this.handleDeleteOption.bind(this);

    //     this.state = {
    //         options: []
    //     }
    // }

    handleDeleteOptions = () => {
        this.setState(() => ({ options: [] }));
    }

    handleDeleteOption = (optionToRemove) => {
        this.setState((prevState) => ({
            options: prevState.options.filter((option) => optionToRemove !== option)
        }));
    }


    handlePick = () => {
        let randomNumber = getRandomArbitrary(0, this.state.options.length);
        console.log(randomNumber);
        //alert(this.state.options[randomNumber]);
        this.setState(() => ({
            selectedOption: this.state.options[randomNumber]
        }))
    }
    
    handleAddOption = (option) => {
        if (!option) {
            return 'Enter valid valut to add item';
        } else if (this.state.options.indexOf(option) > -1) {
            return 'This option already exists!';
        } 

        this.setState((prevState) => ({ options: prevState.options.concat(option) }));
    }

    handleSelectedOption = () => {
        this.setState(() => ({
            selectedOption: undefined
        }));
    }

    render () {
        const title = 'Indecision';
        const subtitle = 'aaasuter';
        return (
            <div>
                <Header title={title} subtitle={subtitle}/>
                <div className="container">
                    <Action 
                        hasOptions={this.state.options.length > 0} 
                        handlePick={this.handlePick} 
                    />
                    <div className="widget">
                        <Options 
                            options={this.state.options}
                            handleDeleteOptions={this.handleDeleteOptions}
                            handleDeleteOption={this.handleDeleteOption}
                        />
                        <AddOption 
                            handleAddOption={this.handleAddOption}
                        />
                        <OptionModal 
                            selectedOption={this.state.selectedOption}
                            handleSelectedOption={this.handleSelectedOption}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

const getRandomArbitrary = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}